Simple Gravatar
===============

Gravatar Mod from Simple Machines Forum 2.0.x

A Globally Recognized Avatar.

[SMF mod site](http://custom.simplemachines.org/mods/index.php?mod=3451) | [Author page](http://simaru.org/index.php?topic=14.0) | [MIT License](http://www.opensource.org/licenses/mit-license.php)