<?php

return array(
	'integrate_pre_include' => '$sourcedir/Gravatar.php',
	'integrate_load_theme' => '\Gravatar\load_theme',
	'integrate_general_mod_settings' => '\Gravatar\general_mod_settings',
	'integrate_modify_modifications' => '\Gravatar\modify_modifications',
	'integrate_load_permissions' => '\Gravatar\load_permissions',
	'integrate_admin_areas' => '\Gravatar\admin_areas',
	'integrate_profile_areas' => '\Gravatar\profile_areas',
);
