<?php

/**
 * @source http://gravatar.com/site/implement/images/php/
 * @author Inter http://simaru.org/
 * @version 1.1.2
 * @license MIT License - http://www.opensource.org/licenses/mit-license.php
 */

namespace Gravatar;

function get($email, $show)
{
    global $modSettings;

    if (!$modSettings['gravatar_enable'])
        return '';

    $protocol = $modSettings['gravatar_transfer_protocol'] === 'http' ? 'http://www.gravatar.com/avatar/' : 'https://secure.gravatar.com/avatar/';

    $url = $protocol . md5(strtolower(trim($email))) . '?s=' . $modSettings['gravatar_max_size'] . '&amp;d=' . $modSettings['gravatar_default_face'] . '&amp;r=' . $modSettings['gravatar_rating'];

    if (!$show)
        return $url;

    return '<img src="' . $url . '" alt="" />';
}

function profile_areas(&$profile_areas)
{
    loadLanguage('ProfileGravatar');
}

function admin_areas(&$admin_areas)
{
    global $txt;

    loadLanguage('AdminGravatar');
    loadLanguage('HelpGravatar');

    $admin_areas['config']['areas']['modsettings']['subsections']['gravatar'] = array($txt['gravatar_title']);
}

function modify_modifications(&$subActions)
{
    $subActions['gravatar'] = '\Gravatar\modifyGravatarSettings';
}

function modifyGravatarSettings($return_config = false)
{
    global $txt, $scripturl, $context;

    $config_vars = array(
        array('title', 'gravatar_title'),
        array('permissions', 'profile_gravatar_avatar', 0, $txt['gravatar_groups_description']),
        array('check', 'gravatar_enable'),
        array('int', 'gravatar_max_size', 6, 'subtext' => $txt['gravatar_max_size_subtext'], 'postinput' => $txt['gravatar_unit']),
        array('select', 'gravatar_default_face', $txt['gravatar_default_faces']),
        array('select', 'gravatar_rating', $txt['gravatar_ratings']),
        array('select', 'gravatar_transfer_protocol', $txt['gravatar_transfer_protocols'],
    ));

    if ($return_config)
        return $config_vars;

    if (isset($_GET['save'])) {
        checkSession();

        foreach (array(
            'gravatar_transfer_protocol',
            'gravatar_max_size',
            'gravatar_default_face',
            'gravatar_rating') as $val
        ) {
            if (!isset($_POST[$val]))
                fatal_error('Field ' . $val . ' Not Found!');
        }

        $_POST['gravatar_transfer_protocol'] = $_POST['gravatar_transfer_protocol'] === 'http' ? 'http' : 'https';

        $_POST['gravatar_max_size'] = (int) $_POST['gravatar_max_size'];
        $_POST['gravatar_max_size'] = empty($_POST['gravatar_max_size']) ? 80 : max(1, min($_POST['gravatar_max_size'], 2048));

        $_POST['gravatar_default_face'] = !empty($_POST['gravatar_default_face']) && in_array($_POST['gravatar_default_face'], array('404', 'mm', 'identicon', 'monsterid', 'wavatar', 'retro')) ? $_POST['gravatar_default_face'] : 'monsterid';

        $_POST['gravatar_rating'] = !empty($_POST['gravatar_rating']) && in_array($_POST['gravatar_rating'], array('g', 'pg', 'r', 'x')) ? $_POST['gravatar_rating'] : 'g';

        saveDBSettings($config_vars);

        writeLog();

        redirectexit('action=admin;area=modsettings;sa=gravatar');
    }

    $context['post_url'] = $scripturl . '?action=admin;area=modsettings;save;sa=gravatar';
    # $context['settings_title'] = $txt['gravatar_title'];

    prepareDBSettingContext($config_vars);
}

function load_permissions(
    &$permissionGroups, &$permissionList, &$leftPermissionGroups, &$hiddenPermissions, &$relabelPermissions
) {
    $permissionList['membergroup'] += array('profile_gravatar_avatar' => array(false, 'profile', 'use_avatar'));
}

function load_theme()
{
    if (isset($_GET['help']))
        loadLanguage('HelpGravatar');
}
